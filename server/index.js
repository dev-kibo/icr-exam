const express = require("express");
const bodyParser = require("body-parser");
const admin = require("firebase-admin");
const serviceAccount = require("./firebase.json");
const { WebhookClient, Payload } = require("dialogflow-fulfillment");

const app = express();

const PORT = 4201;

const firebaseApp = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL:
    "https://icr-shop-default-rtdb.europe-west1.firebasedatabase.app",
});

const firestore = firebaseApp.firestore();

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});

// middleware
app.use(bodyParser.json());

app.post("/hook", (request, response) => {
  const agent = new WebhookClient({
    request,
    response,
  });

  async function sendResponse(agent) {
    const intent = agent.intent;
    const snapshot = await firestore.collection("products").get();
    let products = snapshot.docs.map((doc) => {
      return { firebaseId: doc.id, ...doc.data() };
    });

    if (intent === "order-product") {
      const userId = agent.originalRequest.payload.userId;

      if (!userId) {
        agent.end("Please sign in to order.");
      } else {
        agent.add("What would you like to order?");
      }
    } else if (intent === "order-product-category") {
      const userId = agent.originalRequest.payload.userId;

      if (!userId) {
        agent.end("Please sign in to order.");
      } else {
        agent.add("What's the product name?");
      }
    } else if (intent === "order-product-name") {
      const userId = agent.originalRequest.payload.userId;
      if (!userId) {
        agent.end("Please sign in to order.");
      } else {
        const context = agent.context.get("product");
        const category = context.parameters["product-type"];
        const name = agent.parameters["product-name"];

        const product = products.find((product) => {
          return (
            product.name.toLowerCase().includes(name.toLowerCase()) &&
            product.category === category
          );
        });

        if (product === undefined || product === null) {
          agent.end("We couldn't find that sticker.");
        }

        const response = createResponse([product]);
        const payload = {
          richContent: response,
        };

        agent.add("Did you mean this one?");
        agent.end(
          new Payload(agent.UNSPECIFIED, payload, {
            sendAsMessage: true,
            rawPayload: true,
          })
        );
      }
    } else if (intent === "order-product-yes") {
      const userId = agent.originalRequest.payload.userId;
      if (!userId) {
        agent.end("Please sign in to order.");
      } else {
        agent.add("Great, how many do you need?");
      }
    } else if (intent === "order-product-quantity") {
      const userId = agent.originalRequest.payload.userId;
      if (!userId) {
        agent.end("Please sign in to order.");
      } else {
        const context = agent.context.get("product");
        const name = context.parameters["product-name"];
        const category = context.parameters["product-type"];
        const quantity = agent.parameters["quantity"];

        const product = products.find((p) => {
          return (
            p.name.toLowerCase().includes(name.toLowerCase()) &&
            category === p.category
          );
        });

        if (quantity > product.stock) {
          agent.end("We don't have that many available.");
        } else {
          const querySnapshot = await firestore
            .collection("users")
            .where("id", "==", userId)
            .get();

          let userDetails;
          querySnapshot.forEach((doc) => {
            userDetails = doc.data();
          });

          firestore.collection("orders").add({
            createdAt: new Date(Date.now()).toUTCString(),
            customerId: userId,
            deliveryAddress: {
              city: userDetails.address.city,
              street: userDetails.address.street,
              streetNum: userDetails.address.streetNumber,
            },
            products: [
              {
                productId: product.firebaseId,
                quantity: quantity,
              },
            ],
            status: "inProgress",
            totalPrice: product.price * quantity,
          });

          agent.end(
            `Awesome, order completed. Expect delivery in ${product.deliveryDays} days`
          );
        }
      }
    } else if (intent === "product-search-name") {
      const query = agent.parameters["product-name"];

      products = products
        .filter((p) => p.name.toLowerCase().startsWith(query.toLowerCase()))
        .slice(0, 3);

      if (products.length === 0) {
        agent.end(`We don't have any available.`);
      } else {
        const result = createResponse(products);
        const response = result.length > 5 ? result.slice(0, 6) : result;
        const payload = {
          richContent: response,
        };

        agent.add("Here's what we found");
        agent.end(
          new Payload(agent.UNSPECIFIED, payload, {
            sendAsMessage: true,
            rawPayload: true,
          })
        );
      }
    } else if (intent === "product-type") {
      const product = agent.parameters["product_type"];

      products = products.filter((p) => p.category === product);

      if (products.length === 0) {
        agent.add("We just sold the last one.");
      } else {
        agent.add(`What sticker size do you need?`);
      }
    } else if (intent === "product-size") {
      const product_type = agent.context.get("product").parameters[
        "product_type"
      ];
      const size = agent.parameters["size"];

      products = products.filter(
        (p) => p.size === size && p.category === product_type
      );

      if (products.length === 0) {
        agent.end(`We don't have any available.`);
      } else {
        agent.add(
          `We found ${products.length} different kind of stickers? How many do you need?`
        );
      }
    } else if (intent === "product-quantity") {
      const product_type = agent.context.get("product").parameters[
        "product_type"
      ];
      const size = agent.context.get("product").parameters["size"];
      const quantity = agent.parameters["quantity"];

      products = products.filter(
        (p) =>
          p.stock >= quantity && p.category === product_type && p.size === size
      );

      if (products.length === 0) {
        agent.end(`We don't have any available.`);
      } else {
        agent.add(`Great. In what price range would you want stickers?`);
      }
    } else if (intent === "product-price") {
      const product_type = agent.context.get("product").parameters[
        "product_type"
      ];
      const size = agent.context.get("product").parameters["size"];
      const quantity = agent.context.get("product").parameters["quantity"];
      const price_min = agent.parameters["price_min"];
      const price_max = agent.parameters["price_max"];

      products = products.filter(
        (p) =>
          p.price >= price_min &&
          p.price <= price_max &&
          p.category === product_type &&
          p.size === size &&
          p.stock >= quantity
      );

      if (products.length === 0) {
        agent.add("We don't have any available");
      } else {
        agent.add("In how many days would you like your stickers to arrive?");
      }
    } else if (intent === "product-delivery") {
      const product_type = agent.context.get("product").parameters[
        "product_type"
      ];
      const size = agent.context.get("product").parameters["size"];
      const quantity = agent.context.get("product").parameters["quantity"];
      const price_min = agent.context.get("product").parameters["price_min"];
      const price_max = agent.context.get("product").parameters["price_max"];
      const delivery = agent.parameters["delivery"];

      products = products.filter(
        (p) =>
          p.deliveryDays === delivery &&
          p.price >= price_min &&
          p.price <= price_max &&
          p.stock >= quantity &&
          p.size === size &&
          p.category === product_type
      );

      if (products.length === 0) {
        agent.add("We don't have any available");
      } else {
        agent.add("Awesome. Here's what we found");
        const result = createResponse(products);
        const response = result.length > 5 ? result.slice(0, 6) : result;
        const payload = {
          richContent: response,
        };

        agent.end(
          new Payload(agent.UNSPECIFIED, payload, {
            sendAsMessage: true,
            rawPayload: true,
          })
        );
      }
    }
    // else if ("product-reviews") {
    //   const product_type = agent.context.get("product").parameters[
    //     "product_type"
    //   ];
    //   const size = agent.context.get("product").parameters["size"];
    //   const quantity = agent.context.get("product").parameters["quantity"];
    //   const price_min = agent.context.get("product").parameters["price_min"];
    //   const price_max = agent.context.get("product").parameters["price_max"];
    //   const delivery = agent.context.get("product").parameters["delivery"];
    //   const rating = agent.parameters["rating"];

    //   products = products.filter(
    //     (p) =>
    //       p.deliveryDays === delivery &&
    //       p.price >= price_min &&
    //       p.price <= price_max &&
    //       p.stock >= quantity &&
    //       p.size === size &&
    //       p.category === product_type
    //   );

    //   if (rating !== 0) {
    //     products = products.filter((p) => {
    //       if (p.reviews) {
    //         const totalRating = products.reduce((total, review) => {
    //           total += review.rating;

    //           return total;
    //         }, 0);

    //         const avgRating = totalRating / p.reviews.length;

    //         if (avgRating === rating) {
    //           return true;
    //         }
    //       }

    //       return false;
    //     });
    //   }

    //   if (products.length === 0) {
    //     agent.add("We don't have any available");
    //   } else {
    //     agent.add("Awesome. Here's what we found");
    //     const result = createResponse(products);
    //     const response = result.length > 5 ? result.slice(0, 6) : result;
    //     const payload = {
    //       richContent: response,
    //     };

    //     agent.end(
    //       new Payload(agent.UNSPECIFIED, payload, {
    //         sendAsMessage: true,
    //         rawPayload: true,
    //       })
    //     );
    //   }
    // }
    else {
      agent.add("Test");
    }
  }

  agent.handleRequest(sendResponse);
});

function createResponse(result) {
  return result.map((r) => {
    return [
      {
        type: "image",
        rawUrl: `${r.images[0].path}`,
      },
      {
        type: "info",
        title: `${r.name}`,
        subtitle: `${r.description.slice(0, 10)}`,
        actionLink: `http://localhost:4200/product/${r.firebaseId}`,
      },
      {
        type: "description",
        title: `$${r.price}`,
        text: [`Size ${r.size} inch`, `Made in ${r.originCountry}`],
      },
      {
        type: "chips",
        options: [
          {
            text: "View",
            link: `http://localhost:4200/product/${r.firebaseId}`,
          },
        ],
      },
    ];
  });
}
