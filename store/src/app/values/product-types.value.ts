const PRODUCT_TYPES: any[] = [
  { name: 'Tablet' },
  { name: 'Laptop' },
  { name: 'Smartphone' },
];

export default PRODUCT_TYPES;
