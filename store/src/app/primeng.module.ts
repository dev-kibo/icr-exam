import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { MenubarModule } from 'primeng/menubar';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { DropdownModule } from 'primeng/dropdown';
import { CardModule } from 'primeng/card';
import { MultiSelectModule } from 'primeng/multiselect';
import { RippleModule } from 'primeng/ripple';
import { ChipsModule } from 'primeng/chips';
import { PasswordModule } from 'primeng/password';
import { CarouselModule } from 'primeng/carousel';
import { TabMenuModule } from 'primeng/tabmenu';
import { CheckboxModule } from 'primeng/checkbox';
import { SliderModule } from 'primeng/slider';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { MenuModule } from 'primeng/menu';
import { GalleriaModule } from 'primeng/galleria';
import { InputNumberModule } from 'primeng/inputnumber';
import { DividerModule } from 'primeng/divider';
import { ProgressBarModule } from 'primeng/progressbar';
import { RatingModule } from 'primeng/rating';
import { ScrollTopModule } from 'primeng/scrolltop';
import { ToastModule } from 'primeng/toast';
import { BadgeModule } from 'primeng/badge';
import { StepsModule } from 'primeng/steps';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TableModule } from 'primeng/table';
import { TagModule } from 'primeng/tag';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  imports: [
    ButtonModule,
    AccordionModule,
    MenubarModule,
    InputTextModule,
    InputMaskModule,
    DropdownModule,
    CardModule,
    MultiSelectModule,
    RippleModule,
    ChipsModule,
    PasswordModule,
    CarouselModule,
    TabMenuModule,
    CheckboxModule,
    SliderModule,
    RadioButtonModule,
    MessagesModule,
    MessageModule,
    OverlayPanelModule,
    MenuModule,
    GalleriaModule,
    InputNumberModule,
    DividerModule,
    ProgressBarModule,
    RatingModule,
    ScrollTopModule,
    ToastModule,
    BadgeModule,
    StepsModule,
    InputSwitchModule,
    TableModule,
    TagModule,
    ConfirmDialogModule,
    InputTextareaModule,
    ProgressSpinnerModule,
    DialogModule,
  ],
  exports: [
    ButtonModule,
    AccordionModule,
    MenubarModule,
    InputTextModule,
    InputMaskModule,
    DropdownModule,
    CardModule,
    MultiSelectModule,
    RippleModule,
    ChipsModule,
    PasswordModule,
    CarouselModule,
    TabMenuModule,
    CheckboxModule,
    SliderModule,
    RadioButtonModule,
    MessagesModule,
    MessageModule,
    OverlayPanelModule,
    MenuModule,
    GalleriaModule,
    InputNumberModule,
    DividerModule,
    ProgressBarModule,
    RatingModule,
    ScrollTopModule,
    ToastModule,
    BadgeModule,
    StepsModule,
    InputSwitchModule,
    TableModule,
    TagModule,
    ConfirmDialogModule,
    InputTextareaModule,
    ProgressSpinnerModule,
    DialogModule,
  ],
})
export class PrimeNgModule {}
