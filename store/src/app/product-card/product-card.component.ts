import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import Product from '../models/product.model';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css'],
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;

  constructor(
    private cartService: CartService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  addToCart(): void {
    this.cartService.addToCart(this.product.firebaseId);
    this.messageService.add({
      severity: 'success',
      summary: 'Cart',
      detail: 'Product added to cart.',
      icon: 'pi pi-shopping-cart',
    });
  }

  get rating() {
    if (this.product.reviews) {
      const total = this.product.reviews.reduce((total, review) => {
        total += review.rating;

        return total;
      }, 0);

      return Math.round(total / this.product.reviews.length);
    }

    return 0;
  }
}
