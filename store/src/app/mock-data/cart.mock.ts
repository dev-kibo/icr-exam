import ShoppingCart from '../models/shopping-cart.model';

const CARTS: ShoppingCart[] = [
  {
    id: 'asdasda-asdasd',
    customerId: 'asdka-asdljasd-sadk-asda',
    totalPrice: 1402.12,
    products: [
      {
        id: '8ebfafb2-1c21-4912-9787-c9f8cda0d399',
        category: 'smartphone',
        name: 'lectus',
        description:
          'orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio',
        images: [
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg==',
          },
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPkOWpUDwADbAGEpnc/9QAAAABJRU5ErkJggg==',
          },
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPkyT1aDwADigG/Izk42wAAAABJRU5ErkJggg==',
          },
        ],
        price: 41.91,
        originCountry: 'Sweden',
        seller: { name: 'Cassie Stainburn', location: 'Georgia' },
        stock: 3814,
        size: 16,
        deliveryDays: 1,
        reviews: [
          {
            customerId: 'asdka-asdljasd-sadk-asda',
            rating: 1,
            comment: 'rutrum nulla nunc purus phasellus in',
            createdAt: '2020-08-21T12:08:55Z',
          },
        ],
        quantity: 2,
      },
      {
        id: 'f395f9c2-1faa-4f9b-bdef-22bbba00da71',
        category: 'laptop',
        name: 'consectetuer',
        description:
          'cras in purus eu magna vulputate luctus cum sociis natoque penatibus',
        images: [
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg==',
          },
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPkOWpUDwADbAGEpnc/9QAAAABJRU5ErkJggg==',
          },
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPkyT1aDwADigG/Izk42wAAAABJRU5ErkJggg==',
          },
        ],
        price: 215.55,
        originCountry: 'China',
        seller: { name: 'Nathanial Cranmere', location: 'France' },
        stock: 9031,
        size: 17,
        deliveryDays: 7,
        reviews: null,
        quantity: 1,
      },
      {
        id: 'f986215d-aa5e-4046-8808-6a1f7340bf15',
        category: 'tablet',
        name: 'eu orci',
        description:
          'quam nec dui luctus rutrum nulla tellus in sagittis dui vel',
        images: [
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg==',
          },
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPkOWpUDwADbAGEpnc/9QAAAABJRU5ErkJggg==',
          },
          {
            name:
              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPkyT1aDwADigG/Izk42wAAAABJRU5ErkJggg==',
          },
        ],
        price: 77.49,
        originCountry: 'Colombia',
        seller: { name: 'Goldarina Sporton', location: 'France' },
        stock: 3980,
        size: 14,
        deliveryDays: 7,
        quantity: 3,
        reviews: [
          {
            customerId: 'asdka-asdljasd-sadk-asda',
            rating: 2,
            comment: 'elementum pellentesque quisque porta',
            createdAt: '2020-10-30T19:47:58Z',
          },
          {
            customerId: 'asdka-asdljasd-sadk-asda',
            rating: 2,
            comment: 'in faucibus',
            createdAt: '2020-02-21T20:40:08Z',
          },
          {
            customerId: 'asdka-asdljasd-sadk-asda',
            rating: 3,
            comment: null,
            createdAt: '2020-03-26T12:10:19Z',
          },
          {
            customerId: 'asdka-asdljasd-sadk-asda',
            rating: 4,
            comment: 'amet consectetuer',
            createdAt: '2020-05-25T18:16:19Z',
          },
          {
            customerId: 'asdka-asdljasd-sadk-asda',
            rating: 3,
            comment: 'at velit',
            createdAt: '2020-05-27T01:47:36Z',
          },
          {
            customerId: 'asdka-asdljasd-sadk-asda',
            rating: 3,
            comment: 'elementum ligula vehicula',
            createdAt: '2020-01-26T11:44:00Z',
          },
          {
            customerId: 'asdka-asdljasd-sadk-asda',
            rating: 1,
            comment: 'pede lobortis ligula sit amet',
            createdAt: '2020-06-11T09:00:19Z',
          },
        ],
      },
    ],
  },
];

export default CARTS;
