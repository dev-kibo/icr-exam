export { PRODUCTS as default };

const PRODUCTS = [
  {
    category: 'smartphone',
    name: 'in congue',
    description:
      'Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 336.15,
    originCountry: 'Philippines',
    seller: { name: 'Connie Alishoner', location: 'China' },
    stock: 2494.24,
    size: 13,
    deliveryDays: 3,
    createdAt: '2020-10-01T17:13:07Z',
  },
  {
    category: 'laptop',
    name: 'donec ut dolor',
    description:
      'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 195.0,
    originCountry: 'South Africa',
    seller: { name: 'Obadiah Fenna', location: 'Malaysia' },
    stock: 1612.06,
    size: 17,
    deliveryDays: 14,
    createdAt: '2020-04-18T08:31:20Z',
  },
  {
    category: 'laptop',
    name: 'eros',
    description:
      'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 222.56,
    originCountry: 'China',
    seller: { name: 'Philipa Edgeler', location: 'China' },
    stock: 3827.38,
    size: 15,
    deliveryDays: 7,

    createdAt: '2020-01-21T17:25:16Z',
  },
  {
    category: 'smartphone',
    name: 'erat id',
    description:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 367.2,
    originCountry: 'Nigeria',
    seller: { name: 'Haywood Colomb', location: 'South Korea' },
    stock: 7954.59,
    size: 11,
    deliveryDays: 7,

    createdAt: '2020-09-26T07:03:43Z',
  },
  {
    category: 'tablet',
    name: 'venenatis',
    description:
      'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 172.55,
    originCountry: 'Argentina',
    seller: { name: 'Agnese De Fraine', location: 'Iran' },
    stock: 3909.01,
    size: 13,
    deliveryDays: 14,

    createdAt: '2020-07-25T06:24:03Z',
  },
  {
    category: 'smartphone',
    name: 'nibh fusce',
    description:
      'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 272.59,
    originCountry: 'Colombia',
    seller: { name: 'Ara Ricardot', location: 'Russia' },
    stock: 8211.95,
    size: 11,
    deliveryDays: 30,

    createdAt: '2020-04-01T14:52:13Z',
  },
  {
    category: 'tablet',
    name: 'amet cursus id',
    description: 'Suspendisse potenti.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 408.39,
    originCountry: 'Portugal',
    seller: { name: 'Avram Pammenter', location: 'Japan' },
    stock: 7957.81,
    size: 14,
    deliveryDays: 3,

    createdAt: '2020-07-11T19:57:19Z',
  },
  {
    category: 'laptop',
    name: 'sed',
    description:
      'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 5.48,
    originCountry: 'Indonesia',
    seller: { name: 'Grove Neilan', location: 'United States' },
    stock: 4865.77,
    size: 15,
    deliveryDays: 30,

    createdAt: '2020-08-06T11:59:22Z',
  },
  {
    category: 'tablet',
    name: 'vel sem',
    description:
      'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 158.91,
    originCountry: 'Peru',
    seller: { name: 'Stacy Seiler', location: 'Colombia' },
    stock: 2684.8,
    size: 13,
    deliveryDays: 7,

    createdAt: '2020-10-09T22:17:54Z',
  },
  {
    category: 'laptop',
    name: 'sed magna',
    description:
      'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 230.06,
    originCountry: 'Sri Lanka',
    seller: { name: 'Bekki Screase', location: 'Russia' },
    stock: 2905.16,
    size: 13,
    deliveryDays: 1,

    createdAt: '2020-03-31T03:59:47Z',
  },
  {
    category: 'smartphone',
    name: 'at nulla suspendisse',
    description:
      'Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 242.2,
    originCountry: 'Philippines',
    seller: { name: 'Randee Crosoer', location: 'Czech Republic' },
    stock: 4704.35,
    size: 14,
    deliveryDays: 7,

    createdAt: '2020-12-06T19:53:14Z',
  },
  {
    category: 'laptop',
    name: 'eget elit',
    description:
      'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 332.0,
    originCountry: 'Tanzania',
    seller: { name: 'Dacy Wardlaw', location: 'Japan' },
    stock: 408.15,
    size: 14,
    deliveryDays: 30,

    createdAt: '2020-12-23T13:51:03Z',
  },
  {
    category: 'laptop',
    name: 'augue vel',
    description: 'In eleifend quam a odio.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 261.85,
    originCountry: 'Czech Republic',
    seller: { name: 'Florry Winters', location: 'Czech Republic' },
    stock: 690.95,
    size: 17,
    deliveryDays: 1,

    createdAt: '2020-03-01T06:27:25Z',
  },
  {
    category: 'tablet',
    name: 'fusce congue diam',
    description:
      'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 195.98,
    originCountry: 'Russia',
    seller: { name: 'Belia Brownsea', location: 'Palestinian Territory' },
    stock: 6033.02,
    size: 13,
    deliveryDays: 1,

    createdAt: '2020-10-11T01:05:48Z',
  },
  {
    category: 'laptop',
    name: 'montes nascetur ridiculus',
    description:
      'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 256.23,
    originCountry: 'Indonesia',
    seller: { name: 'Celeste Pavlata', location: 'Brazil' },
    stock: 1628.71,
    size: 11,
    deliveryDays: 30,

    createdAt: '2020-05-21T20:02:42Z',
  },
  {
    category: 'laptop',
    name: 'tortor eu',
    description:
      'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 44.27,
    originCountry: 'China',
    seller: { name: 'Lesya Petrasch', location: 'Philippines' },
    stock: 5968.47,
    size: 17,
    deliveryDays: 3,

    createdAt: '2020-05-18T01:12:28Z',
  },
  {
    category: 'tablet',
    name: 'in faucibus',
    description:
      'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 329.86,
    originCountry: 'France',
    seller: { name: 'Morgen De Haven', location: 'Indonesia' },
    stock: 4253.76,
    size: 15,
    deliveryDays: 1,

    createdAt: '2020-04-12T21:32:44Z',
  },
  {
    category: 'smartphone',
    name: 'penatibus et magnis',
    description:
      'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 285.43,
    originCountry: 'Indonesia',
    seller: { name: 'Isidro Abrahamson', location: 'Russia' },
    stock: 6325.49,
    size: 17,
    deliveryDays: 14,

    createdAt: '2020-07-01T04:24:25Z',
  },
  {
    category: 'laptop',
    name: 'augue a',
    description:
      'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 206.75,
    originCountry: 'China',
    seller: { name: 'Jemmie Truswell', location: 'United States' },
    stock: 7717.61,
    size: 13,
    deliveryDays: 30,

    createdAt: '2020-03-31T22:18:38Z',
  },
  {
    category: 'tablet',
    name: 'vivamus',
    description:
      'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 332.8,
    originCountry: 'United States',
    seller: { name: 'Sylvia Brear', location: 'Mongolia' },
    stock: 7962.71,
    size: 11,
    deliveryDays: 30,

    createdAt: '2020-12-11T08:26:25Z',
  },
  {
    category: 'laptop',
    name: 'mi nulla ac',
    description:
      'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 172.32,
    originCountry: 'China',
    seller: { name: 'Josephina Rosterne', location: 'Argentina' },
    stock: 8937.52,
    size: 14,
    deliveryDays: 30,

    createdAt: '2020-10-22T20:53:17Z',
  },
  {
    category: 'smartphone',
    name: 'in hac habitasse',
    description:
      'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 397.88,
    originCountry: 'Thailand',
    seller: { name: 'Gwenneth Nitti', location: 'Argentina' },
    stock: 8227.92,
    size: 14,
    deliveryDays: 1,

    createdAt: '2020-07-03T04:44:14Z',
  },
  {
    category: 'smartphone',
    name: 'mi pede malesuada',
    description: 'Sed ante. Vivamus tortor.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 408.5,
    originCountry: 'Malaysia',
    seller: { name: 'Lorettalorna Slegg', location: 'Argentina' },
    stock: 6465.05,
    size: 14,
    deliveryDays: 3,

    createdAt: '2020-08-29T05:21:02Z',
  },
  {
    category: 'tablet',
    name: 'erat curabitur',
    description: 'Suspendisse ornare consequat lectus.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 266.98,
    originCountry: 'Pakistan',
    seller: { name: 'Janeta Chanders', location: 'Mexico' },
    stock: 8831.95,
    size: 12,
    deliveryDays: 1,

    createdAt: '2020-01-14T18:57:17Z',
  },
  {
    category: 'tablet',
    name: 'sed tristique in',
    description:
      'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 429.13,
    originCountry: 'Canada',
    seller: { name: 'Kaile Celle', location: 'Poland' },
    stock: 6776.04,
    size: 12,
    deliveryDays: 30,

    createdAt: '2020-04-17T15:18:47Z',
  },
  {
    category: 'tablet',
    name: 'accumsan odio',
    description:
      'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 308.07,
    originCountry: 'Uzbekistan',
    seller: { name: 'Lebbie Arton', location: 'Portugal' },
    stock: 9148.13,
    size: 12,
    deliveryDays: 14,

    createdAt: '2020-08-10T00:15:56Z',
  },
  {
    category: 'tablet',
    name: 'ac nulla',
    description:
      'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 289.68,
    originCountry: 'China',
    seller: { name: 'Skye Pargetter', location: 'China' },
    stock: 859.11,
    size: 13,
    deliveryDays: 1,

    createdAt: '2020-10-30T03:15:06Z',
  },
  {
    category: 'laptop',
    name: 'tempus',
    description:
      'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 239.01,
    originCountry: 'Indonesia',
    seller: { name: 'Janis Petrus', location: 'Philippines' },
    stock: 9396.92,
    size: 13,
    deliveryDays: 7,

    createdAt: '2020-07-27T10:26:53Z',
  },
  {
    category: 'tablet',
    name: 'donec posuere',
    description:
      'Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 481.6,
    originCountry: 'United States',
    seller: { name: 'Rica Van Waadenburg', location: 'Lithuania' },
    stock: 80.76,
    size: 17,
    deliveryDays: 1,

    createdAt: '2020-08-13T15:26:18Z',
  },
  {
    category: 'tablet',
    name: 'at nunc commodo',
    description:
      'Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 341.45,
    originCountry: 'Nicaragua',
    seller: { name: 'Annette Scarrott', location: 'Poland' },
    stock: 3612.59,
    size: 12,
    deliveryDays: 1,

    createdAt: '2020-07-15T14:08:56Z',
  },
  {
    category: 'laptop',
    name: 'donec odio',
    description:
      'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 291.17,
    originCountry: 'China',
    seller: { name: 'Marleen Spere', location: 'France' },
    stock: 2193.27,
    size: 15,
    deliveryDays: 3,

    createdAt: '2020-03-08T16:42:30Z',
  },
  {
    category: 'smartphone',
    name: 'mus',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 148.06,
    originCountry: 'China',
    seller: { name: 'Elita Twelve', location: 'Sri Lanka' },
    stock: 9218.7,
    size: 11,
    deliveryDays: 1,

    createdAt: '2020-08-10T22:55:45Z',
  },
  {
    category: 'tablet',
    name: 'ac',
    description:
      'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 440.36,
    originCountry: 'Indonesia',
    seller: { name: 'Aldus Eades', location: 'China' },
    stock: 1032.35,
    size: 12,
    deliveryDays: 14,

    createdAt: '2020-09-10T14:26:36Z',
  },
  {
    category: 'tablet',
    name: 'aenean auctor',
    description: 'Nulla mollis molestie lorem.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 325.42,
    originCountry: 'Peru',
    seller: { name: 'Terra Wendover', location: 'China' },
    stock: 5817.04,
    size: 15,
    deliveryDays: 14,

    createdAt: '2020-02-12T03:26:29Z',
  },
  {
    category: 'laptop',
    name: 'neque',
    description:
      'Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 300.91,
    originCountry: 'Philippines',
    seller: { name: 'Harley Priel', location: 'Brazil' },
    stock: 7697.19,
    size: 12,
    deliveryDays: 14,

    createdAt: '2020-03-18T16:01:25Z',
  },
  {
    category: 'smartphone',
    name: 'justo',
    description:
      'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 8.3,
    originCountry: 'China',
    seller: { name: 'Aindrea Lotterington', location: 'Indonesia' },
    stock: 7167.21,
    size: 14,
    deliveryDays: 1,

    createdAt: '2020-10-17T05:35:13Z',
  },
  {
    category: 'laptop',
    name: 'adipiscing elit proin',
    description:
      'In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 186.89,
    originCountry: 'Bangladesh',
    seller: { name: 'Bethena Worral', location: 'China' },
    stock: 5964.22,
    size: 14,
    deliveryDays: 1,

    createdAt: '2020-02-18T06:47:31Z',
  },
  {
    category: 'tablet',
    name: 'proin at turpis',
    description:
      'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 29.83,
    originCountry: 'Sweden',
    seller: { name: 'Meier Asbrey', location: 'Portugal' },
    stock: 9195.06,
    size: 13,
    deliveryDays: 1,

    createdAt: '2020-07-09T07:05:59Z',
  },
  {
    category: 'tablet',
    name: 'sit amet eros',
    description: 'Mauris ullamcorper purus sit amet nulla.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 265.8,
    originCountry: 'Vietnam',
    seller: { name: 'Wallis Palfreman', location: 'Albania' },
    stock: 6477.97,
    size: 13,
    deliveryDays: 7,

    createdAt: '2020-05-01T01:31:51Z',
  },
  {
    category: 'tablet',
    name: 'at lorem',
    description:
      'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 121.21,
    originCountry: 'Portugal',
    seller: { name: 'Yurik Surgey', location: 'Philippines' },
    stock: 1769.97,
    size: 13,
    deliveryDays: 30,

    createdAt: '2020-02-16T11:15:44Z',
  },
  {
    category: 'laptop',
    name: 'amet',
    description:
      'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 159.95,
    originCountry: 'Venezuela',
    seller: { name: 'Moshe Tape', location: 'China' },
    stock: 6035.98,
    size: 12,
    deliveryDays: 1,

    createdAt: '2020-12-27T23:40:00Z',
  },
  {
    category: 'smartphone',
    name: 'sit',
    description:
      'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 126.15,
    originCountry: 'Greece',
    seller: { name: 'Elene Skotcher', location: 'Gambia' },
    stock: 7722.2,
    size: 17,
    deliveryDays: 14,

    createdAt: '2020-04-29T21:03:38Z',
  },
  {
    category: 'smartphone',
    name: 'cras',
    description:
      'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 136.27,
    originCountry: 'Chile',
    seller: { name: 'Kale Strewther', location: 'Greece' },
    stock: 935.96,
    size: 12,
    deliveryDays: 1,

    createdAt: '2020-01-15T14:09:58Z',
  },
  {
    category: 'tablet',
    name: 'lacus morbi quis',
    description:
      'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 62.81,
    originCountry: 'China',
    seller: { name: 'Brandice Haswell', location: 'Nicaragua' },
    stock: 6320.03,
    size: 15,
    deliveryDays: 3,

    createdAt: '2020-02-22T10:20:44Z',
  },
  {
    category: 'laptop',
    name: 'turpis elementum',
    description:
      'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 206.57,
    originCountry: 'Portugal',
    seller: { name: 'Onida Murgatroyd', location: 'Serbia' },
    stock: 6609.51,
    size: 15,
    deliveryDays: 7,

    createdAt: '2020-05-24T21:40:40Z',
  },
  {
    category: 'tablet',
    name: 'at',
    description:
      'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 27.02,
    originCountry: 'Canada',
    seller: { name: 'Lelah Odo', location: 'Ukraine' },
    stock: 946.13,
    size: 17,
    deliveryDays: 30,

    createdAt: '2020-09-11T17:10:10Z',
  },
  {
    category: 'smartphone',
    name: 'sit amet lobortis',
    description:
      'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 246.88,
    originCountry: 'China',
    seller: { name: 'Luke Doyley', location: 'Afghanistan' },
    stock: 4178.43,
    size: 17,
    deliveryDays: 7,

    createdAt: '2020-09-22T17:53:07Z',
  },
  {
    category: 'laptop',
    name: 'orci',
    description:
      'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 165.86,
    originCountry: 'Finland',
    seller: { name: 'Joeann Lessmare', location: 'Ukraine' },
    stock: 9254.14,
    size: 17,
    deliveryDays: 14,

    createdAt: '2020-03-29T15:28:18Z',
  },
  {
    category: 'laptop',
    name: 'eu felis',
    description:
      'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 118.12,
    originCountry: 'Peru',
    seller: { name: 'Bord Storrah', location: 'Sweden' },
    stock: 8605.5,
    size: 11,
    deliveryDays: 7,

    createdAt: '2020-07-31T07:32:43Z',
  },
  {
    category: 'laptop',
    name: 'posuere metus',
    description:
      'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 348.44,
    originCountry: 'China',
    seller: { name: 'Yehudi Perdue', location: 'Venezuela' },
    stock: 9250.42,
    size: 15,
    deliveryDays: 7,

    createdAt: '2021-01-03T23:14:44Z',
  },
  {
    category: 'tablet',
    name: 'fusce',
    description:
      'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 380.99,
    originCountry: 'Philippines',
    seller: { name: 'Sascha Merida', location: 'Czech Republic' },
    stock: 9785.6,
    size: 14,
    deliveryDays: 7,

    createdAt: '2020-04-08T23:31:17Z',
  },
  {
    category: 'smartphone',
    name: 'nec',
    description:
      'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 169.16,
    originCountry: 'Peru',
    seller: { name: 'Maressa Chadbourne', location: 'China' },
    stock: 7394.31,
    size: 13,
    deliveryDays: 30,

    createdAt: '2020-10-20T22:05:27Z',
  },
  {
    category: 'smartphone',
    name: 'molestie',
    description:
      'Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 25.17,
    originCountry: 'Iran',
    seller: { name: 'Saundra Clemmey', location: 'Nigeria' },
    stock: 9900.24,
    size: 15,
    deliveryDays: 30,

    createdAt: '2020-04-24T18:58:27Z',
  },
  {
    category: 'smartphone',
    name: 'dolor sit amet',
    description: 'Vestibulum rutrum rutrum neque.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 338.07,
    originCountry: 'Honduras',
    seller: { name: 'Monah Pealing', location: 'China' },
    stock: 7800.16,
    size: 14,
    deliveryDays: 7,

    createdAt: '2020-09-18T05:42:26Z',
  },
  {
    category: 'laptop',
    name: 'non',
    description: 'Nulla nisl.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 379.18,
    originCountry: 'Poland',
    seller: { name: 'Tabbitha Padgham', location: 'Brazil' },
    stock: 1760.44,
    size: 11,
    deliveryDays: 30,

    createdAt: '2020-09-01T22:10:47Z',
  },
  {
    category: 'tablet',
    name: 'lobortis ligula',
    description:
      'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 116.94,
    originCountry: 'Indonesia',
    seller: { name: 'Zollie Witsey', location: 'Greece' },
    stock: 8103.92,
    size: 14,
    deliveryDays: 1,

    createdAt: '2020-08-16T15:17:52Z',
  },
  {
    category: 'tablet',
    name: 'at nulla suspendisse',
    description:
      'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 142.59,
    originCountry: 'Brazil',
    seller: { name: 'Derril Huckin', location: 'Myanmar' },
    stock: 9419.16,
    size: 14,
    deliveryDays: 30,

    createdAt: '2020-04-28T14:08:10Z',
  },
  {
    category: 'tablet',
    name: 'eleifend quam a',
    description:
      'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 436.7,
    originCountry: 'China',
    seller: { name: 'Karon Garaghan', location: 'Poland' },
    stock: 5974.37,
    size: 17,
    deliveryDays: 3,

    createdAt: '2020-12-10T03:30:08Z',
  },
  {
    category: 'tablet',
    name: 'aenean auctor',
    description:
      'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 293.35,
    originCountry: 'Indonesia',
    seller: { name: 'Dannye Egentan', location: 'Cape Verde' },
    stock: 9840.98,
    size: 14,
    deliveryDays: 30,

    createdAt: '2020-02-17T23:38:44Z',
  },
  {
    category: 'laptop',
    name: 'eget tempus',
    description:
      'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 49.09,
    originCountry: 'Indonesia',
    seller: { name: 'Madlin Critten', location: 'Mongolia' },
    stock: 7752.02,
    size: 11,
    deliveryDays: 1,

    createdAt: '2020-10-10T12:09:49Z',
  },
  {
    category: 'smartphone',
    name: 'id nulla ultrices',
    description:
      'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 289.2,
    originCountry: 'Italy',
    seller: { name: 'Rolf Fishpool', location: 'Nicaragua' },
    stock: 6234.84,
    size: 15,
    deliveryDays: 7,

    createdAt: '2020-10-06T05:06:43Z',
  },
  {
    category: 'tablet',
    name: 'elit ac',
    description: 'Aenean sit amet justo.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 15.52,
    originCountry: 'Indonesia',
    seller: { name: 'Hanson Glasscoe', location: 'Indonesia' },
    stock: 283.75,
    size: 14,
    deliveryDays: 1,

    createdAt: '2020-09-13T03:33:20Z',
  },
  {
    category: 'smartphone',
    name: 'vestibulum velit id',
    description:
      'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 332.79,
    originCountry: 'Argentina',
    seller: { name: 'Lucias Bazek', location: 'China' },
    stock: 7039.43,
    size: 11,
    deliveryDays: 3,

    createdAt: '2020-06-24T09:41:33Z',
  },
  {
    category: 'laptop',
    name: 'diam nam tristique',
    description:
      'Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 43.09,
    originCountry: 'Indonesia',
    seller: { name: 'Wolf Spurgeon', location: 'Portugal' },
    stock: 7876.89,
    size: 14,
    deliveryDays: 1,

    createdAt: '2020-11-04T04:54:57Z',
  },
  {
    category: 'laptop',
    name: 'volutpat',
    description:
      'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 191.9,
    originCountry: 'China',
    seller: { name: 'Roderick Stadding', location: 'Poland' },
    stock: 8369.32,
    size: 14,
    deliveryDays: 14,

    createdAt: '2020-07-06T17:27:15Z',
  },
  {
    category: 'smartphone',
    name: 'maecenas ut massa',
    description:
      'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 95.29,
    originCountry: 'Poland',
    seller: { name: 'Sloan McCourtie', location: 'Thailand' },
    stock: 6576.88,
    size: 12,
    deliveryDays: 3,

    createdAt: '2020-05-23T05:39:27Z',
  },
  {
    category: 'laptop',
    name: 'sagittis dui vel',
    description:
      'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 251.46,
    originCountry: 'Portugal',
    seller: { name: 'Ody Rochester', location: 'Venezuela' },
    stock: 6446.65,
    size: 11,
    deliveryDays: 14,

    createdAt: '2020-10-11T23:12:46Z',
  },
  {
    category: 'laptop',
    name: 'sed vestibulum',
    description:
      'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 488.07,
    originCountry: 'Sweden',
    seller: { name: 'Benedikt Halvorsen', location: 'Cuba' },
    stock: 7002.98,
    size: 17,
    deliveryDays: 30,

    createdAt: '2021-01-08T19:03:51Z',
  },
  {
    category: 'tablet',
    name: 'dis parturient montes',
    description:
      'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 374.56,
    originCountry: 'Indonesia',
    seller: { name: 'Gunar Everist', location: 'United States' },
    stock: 5595.09,
    size: 17,
    deliveryDays: 3,

    createdAt: '2020-10-14T06:07:48Z',
  },
  {
    category: 'smartphone',
    name: 'ultrices',
    description:
      'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 168.08,
    originCountry: 'Tonga',
    seller: { name: 'Marmaduke Stayte', location: 'China' },
    stock: 3736.23,
    size: 11,
    deliveryDays: 14,

    createdAt: '2020-09-14T14:57:07Z',
  },
  {
    category: 'smartphone',
    name: 'cubilia curae nulla',
    description:
      'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 131.76,
    originCountry: 'Japan',
    seller: { name: 'Jacquenette Lillee', location: 'Russia' },
    stock: 2776.08,
    size: 13,
    deliveryDays: 1,

    createdAt: '2020-04-07T10:32:37Z',
  },
  {
    category: 'smartphone',
    name: 'lacinia erat vestibulum',
    description:
      'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 208.87,
    originCountry: 'Norway',
    seller: { name: 'Ian Grimley', location: 'China' },
    stock: 3030.73,
    size: 11,
    deliveryDays: 14,

    createdAt: '2020-12-25T11:35:04Z',
  },
  {
    category: 'tablet',
    name: 'justo sit',
    description: 'Praesent blandit.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 183.2,
    originCountry: 'China',
    seller: { name: 'Humphrey Elven', location: 'Micronesia' },
    stock: 8251.17,
    size: 13,
    deliveryDays: 14,

    createdAt: '2020-07-23T16:33:04Z',
  },
  {
    category: 'laptop',
    name: 'suspendisse potenti',
    description:
      'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 15.81,
    originCountry: 'Portugal',
    seller: { name: 'Christen Camili', location: 'Ukraine' },
    stock: 4978.34,
    size: 14,
    deliveryDays: 1,

    createdAt: '2020-12-26T23:26:42Z',
  },
  {
    category: 'tablet',
    name: 'in tempus',
    description:
      'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 193.4,
    originCountry: 'Madagascar',
    seller: { name: 'Gene Whitewood', location: 'Indonesia' },
    stock: 9650.23,
    size: 11,
    deliveryDays: 30,

    createdAt: '2020-12-12T15:04:41Z',
  },
  {
    category: 'tablet',
    name: 'maecenas rhoncus',
    description:
      'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 446.56,
    originCountry: 'Philippines',
    seller: { name: 'Orrin Snoxill', location: 'Indonesia' },
    stock: 6942.55,
    size: 15,
    deliveryDays: 14,

    createdAt: '2020-10-02T06:24:39Z',
  },
  {
    category: 'tablet',
    name: 'erat',
    description:
      'Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 348.75,
    originCountry: 'Indonesia',
    seller: { name: 'Jeremie Trump', location: 'Philippines' },
    stock: 9233.86,
    size: 17,
    deliveryDays: 3,

    createdAt: '2020-07-02T17:08:46Z',
  },
  {
    category: 'laptop',
    name: 'duis',
    description:
      'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 97.71,
    originCountry: 'Hungary',
    seller: { name: 'Onofredo Crosland', location: 'Peru' },
    stock: 7536.86,
    size: 13,
    deliveryDays: 30,

    createdAt: '2020-08-21T11:47:28Z',
  },
  {
    category: 'tablet',
    name: 'eget tempus',
    description: 'Curabitur in libero ut massa volutpat convallis.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 111.38,
    originCountry: 'Indonesia',
    seller: { name: 'Olga Sille', location: 'Guatemala' },
    stock: 6342.01,
    size: 14,
    deliveryDays: 7,

    createdAt: '2021-01-12T06:36:19Z',
  },
  {
    category: 'laptop',
    name: 'dictumst',
    description:
      'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 124.17,
    originCountry: 'Sweden',
    seller: { name: 'Jami Klimentyev', location: 'China' },
    stock: 6356.74,
    size: 11,
    deliveryDays: 14,

    createdAt: '2020-03-19T19:45:23Z',
  },
  {
    category: 'laptop',
    name: 'amet',
    description:
      'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 25.99,
    originCountry: 'Philippines',
    seller: { name: 'Dionis Grzelak', location: 'China' },
    stock: 5995.08,
    size: 12,
    deliveryDays: 3,

    createdAt: '2020-08-29T13:10:54Z',
  },
  {
    category: 'smartphone',
    name: 'justo morbi ut',
    description: 'Integer non velit.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 384.23,
    originCountry: 'Bolivia',
    seller: { name: 'Bartlet Senecaux', location: 'Portugal' },
    stock: 6471.35,
    size: 14,
    deliveryDays: 3,

    createdAt: '2020-10-05T01:48:56Z',
  },
  {
    category: 'laptop',
    name: 'ultrices',
    description: 'In congue.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 295.97,
    originCountry: 'Indonesia',
    seller: { name: 'Beatrix Greeves', location: 'Philippines' },
    stock: 2027.42,
    size: 13,
    deliveryDays: 14,

    createdAt: '2020-08-16T11:59:15Z',
  },
  {
    category: 'laptop',
    name: 'semper sapien',
    description:
      'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 85.04,
    originCountry: 'China',
    seller: { name: 'Abelard Burcombe', location: 'Vietnam' },
    stock: 2203.03,
    size: 15,
    deliveryDays: 1,

    createdAt: '2021-01-11T21:29:12Z',
  },
  {
    category: 'smartphone',
    name: 'semper',
    description:
      'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 60.58,
    originCountry: 'Thailand',
    seller: { name: 'Randie Morilla', location: 'China' },
    stock: 2908.79,
    size: 12,
    deliveryDays: 1,

    createdAt: '2020-05-26T20:05:56Z',
  },
  {
    category: 'laptop',
    name: 'turpis integer aliquet',
    description: 'Proin at turpis a pede posuere nonummy.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 459.97,
    originCountry: 'France',
    seller: { name: 'Abey Mollindinia', location: 'Ukraine' },
    stock: 9711.4,
    size: 14,
    deliveryDays: 1,

    createdAt: '2021-01-13T18:38:23Z',
  },
  {
    category: 'smartphone',
    name: 'nullam',
    description:
      'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 396.0,
    originCountry: 'Brazil',
    seller: { name: 'Welby Rodrigo', location: 'Peru' },
    stock: 8992.43,
    size: 11,
    deliveryDays: 3,

    createdAt: '2020-08-07T06:07:43Z',
  },
  {
    category: 'tablet',
    name: 'luctus et',
    description: 'Maecenas pulvinar lobortis est.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 454.47,
    originCountry: 'Cameroon',
    seller: { name: 'Ree Merigeau', location: 'Sweden' },
    stock: 3017.4,
    size: 13,
    deliveryDays: 1,

    createdAt: '2020-06-16T15:49:48Z',
  },
  {
    category: 'smartphone',
    name: 'sollicitudin ut suscipit',
    description:
      'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 55.55,
    originCountry: 'Mauritius',
    seller: { name: 'Sloan Hanton', location: 'China' },
    stock: 5360.91,
    size: 17,
    deliveryDays: 7,

    createdAt: '2020-06-01T03:03:18Z',
  },
  {
    category: 'tablet',
    name: 'iaculis',
    description:
      'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 188.84,
    originCountry: 'Belarus',
    seller: { name: 'Emmalynn Lippard', location: 'China' },
    stock: 1847.36,
    size: 12,
    deliveryDays: 1,

    createdAt: '2020-05-10T03:54:05Z',
  },
  {
    category: 'smartphone',
    name: 'convallis',
    description:
      'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 2.56,
    originCountry: 'Ethiopia',
    seller: { name: 'Silvester Perillo', location: 'Armenia' },
    stock: 264.42,
    size: 12,
    deliveryDays: 1,

    createdAt: '2020-09-20T04:06:21Z',
  },
  {
    category: 'smartphone',
    name: 'parturient',
    description:
      'Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 275.85,
    originCountry: 'China',
    seller: { name: 'Laina Balk', location: 'Philippines' },
    stock: 7221.93,
    size: 14,
    deliveryDays: 7,

    createdAt: '2020-11-25T03:53:25Z',
  },
  {
    category: 'smartphone',
    name: 'lacus curabitur',
    description:
      'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 417.56,
    originCountry: 'Brazil',
    seller: { name: 'Fergus Salomon', location: 'Serbia' },
    stock: 3361.65,
    size: 12,
    deliveryDays: 7,

    createdAt: '2020-09-19T17:33:58Z',
  },
  {
    category: 'tablet',
    name: 'interdum venenatis turpis',
    description:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 131.69,
    originCountry: 'Indonesia',
    seller: { name: 'Hobey Greensmith', location: 'China' },
    stock: 95.41,
    size: 13,
    deliveryDays: 14,

    createdAt: '2020-07-11T21:51:53Z',
  },
  {
    category: 'tablet',
    name: 'turpis integer aliquet',
    description: 'Nulla ut erat id mauris vulputate elementum. Nullam varius.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 192.32,
    originCountry: 'Indonesia',
    seller: { name: 'Noelle Hansley', location: 'Indonesia' },
    stock: 9438.42,
    size: 15,
    deliveryDays: 30,

    createdAt: '2020-03-18T14:33:05Z',
  },
  {
    category: 'tablet',
    name: 'consequat',
    description:
      'Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 270.24,
    originCountry: 'China',
    seller: { name: 'Tasia Bloss', location: 'Poland' },
    stock: 8214.12,
    size: 17,
    deliveryDays: 14,

    createdAt: '2020-06-10T20:11:24Z',
  },
  {
    category: 'tablet',
    name: 'velit vivamus',
    description:
      'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 282.74,
    originCountry: 'Indonesia',
    seller: { name: 'Antonie Van Der Vlies', location: 'Syria' },
    stock: 6343.26,
    size: 12,
    deliveryDays: 7,

    createdAt: '2020-08-11T01:36:37Z',
  },
  {
    category: 'tablet',
    name: 'semper',
    description:
      'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.',
    images: [
      { path: '/assets/phone-stickers/phone-sticker-1.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-2.jpg' },
      { path: '/assets/phone-stickers/phone-sticker-3.jpg' },
    ],
    price: 23.63,
    originCountry: 'Indonesia',
    seller: { name: 'Shara Colthard', location: 'Belarus' },
    stock: 4149.23,
    size: 11,
    deliveryDays: 1,

    createdAt: '2020-08-12T04:08:47Z',
  },
  {
    category: 'smartphone',
    name: 'eleifend pede libero',
    description:
      'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
    images: [
      { path: '/assets/tablet-stickers/tablet-sticker-1.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-2.jpg' },
      { path: '/assets/tablet-stickers/tablet-sticker-3.jpg' },
    ],
    price: 179.33,
    originCountry: 'Ecuador',
    seller: { name: 'Mallory Gimeno', location: 'Indonesia' },
    stock: 7727.33,
    size: 13,
    deliveryDays: 3,

    createdAt: '2020-11-27T12:38:00Z',
  },
  {
    category: 'laptop',
    name: 'quam',
    description:
      'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.',
    images: [
      { path: '/assets/laptop-stickers/laptop-sticker-1.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-2.jpg' },
      { path: '/assets/laptop-stickers/laptop-sticker-3.jpg' },
    ],
    price: 262.84,
    originCountry: 'Indonesia',
    seller: { name: 'Kacey Diche', location: 'Chile' },
    stock: 4047.03,
    size: 15,
    deliveryDays: 1,

    createdAt: '2020-10-23T14:44:02Z',
  },
];
