import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import City from '../models/city.model';
import Customer from '../models/customer.model';
import { AuthService } from '../services/auth.service';
import CITIES from '../values/city.value';
import PRODUCT_TYPES from '../values/product-types.value';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  user;

  isLoading = true;
  isEditProfile = false;

  constructor(
    private authService: AuthService,
    private firestore: AngularFirestore
  ) {}

  ngOnInit(): void {
    this.authService.userDetails.subscribe((user) => {
      this.user = user[0];
      this.isLoading = false;
      console.log(this.user);
    });
  }

  onEdit() {
    this.isEditProfile = true;
  }

  onUpdate(update) {
    const userId = this.authService.user.uid;

    this.firestore
      .collection('users', (ref) => ref.where('id', '==', userId))
      .get()
      .subscribe((snapshots) => {
        const snapshot = snapshots.docs[0];

        const userRef = snapshot.id;

        this.firestore
          .collection('users')
          .doc(userRef)
          .update({
            ...update,
          });

        this.isEditProfile = false;
      });
  }

  getProductIcon(productName: string): string {
    switch (productName.toLowerCase()) {
      case 'smartphone':
        return 'mobile';
      case 'laptop':
        return 'desktop';
      case 'tablet':
        return 'tablet';
    }
  }

  get firstName() {
    return this.user.firstName;
  }

  get lastName() {
    return this.user.lastName;
  }

  get email() {
    return this.authService.user.email;
  }

  get phone() {
    return this.user.phone;
  }

  get city() {
    return this.user.address.city;
  }

  get street() {
    return this.user.address.street;
  }

  get streetNumber() {
    return this.user.address.streetNumber;
  }

  get favoriteProducts() {
    return this.user.favoriteProducts;
  }
}
