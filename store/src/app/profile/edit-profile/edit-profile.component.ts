import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import City from 'src/app/models/city.model';
import CITIES from 'src/app/values/city.value';
import PRODUCT_TYPES from 'src/app/values/product-types.value';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
})
export class EditProfileComponent implements OnInit {
  cities: City[];
  products: string[];
  profileForm;
  @Input() user;
  @Output() onProfileUpdate = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    this.cities = CITIES;
    this.products = PRODUCT_TYPES;

    this.profileForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, Validators.required),
      lastName: new FormControl(this.user.lastName, Validators.required),
      phone: new FormControl(this.user.phone, Validators.required),
      city: new FormControl(this.user.address.city, Validators.required),
      street: new FormControl(this.user.address.street, Validators.required),
      streetNum: new FormControl(
        this.user.address.streetNumber,
        Validators.required
      ),
      favProducts: new FormControl(
        this.user.favoriteProducts,
        Validators.required
      ),
    });
  }

  onUpdate() {
    const update = {
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      phone: this.phone.value,
      city: this.city.value,
      street: this.street.value,
      streetNumber: this.streetNum.value,
      favoriteProducts: this.favProducts.value,
    };

    this.onProfileUpdate.emit(update);
  }

  get firstName() {
    return this.profileForm.get('firstName');
  }

  get lastName() {
    return this.profileForm.get('lastName');
  }

  get phone() {
    return this.profileForm.get('phone');
  }

  get city() {
    return this.profileForm.get('city');
  }

  get street() {
    return this.profileForm.get('street');
  }

  get streetNum() {
    return this.profileForm.get('streetNum');
  }

  get favProducts() {
    return this.profileForm.get('favProducts');
  }
}
