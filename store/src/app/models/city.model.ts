export default interface City {
  name: string;
  zip: number;
}
