export default interface Seller {
  name: string;
  location: string;
}
