import Review from './review.model';
import Seller from './seller.model';

export default interface Product {
  firebaseId: string;
  category: 'laptop' | 'tablet' | 'smartphone';
  name: string;
  description: string;
  images: any[];
  price: number;
  originCountry: string;
  seller: Seller;
  stock: number;
  size: number;
  deliveryDays: number;
  reviews: Review[];
  createdAt: string;
}
