import CartProduct from './cart-product.model';

export default interface ShoppingCart {
  customerId?: string;
  products?: CartProduct[];
  totalPrice?: number;
}
