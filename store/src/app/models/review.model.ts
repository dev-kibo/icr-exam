export default interface Review {
  id?: string;
  customerId: string;
  rating: number;
  comment?: string;
  createdAt: string;
}
