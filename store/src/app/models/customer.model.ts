import Address from './address.model';
import Order from './order.model';

export default interface Customer {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
  address: Address;
  favoriteProducts: any[];
  orders?: Order[];
}
