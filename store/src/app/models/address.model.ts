import City from './city.model';

export default interface Address {
  street: string;
  streetNumber: string;
  city: City;
}
