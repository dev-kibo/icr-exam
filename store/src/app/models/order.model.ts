import { OrderProduct } from './order-product.model';

export default interface Order {
  id?: string;
  customerId: string;
  products: OrderProduct[];
  totalPrice: number;
  createdAt: string;
  status: 'finished' | 'inProgress' | 'cancelled';
  deliveryAddress?: Address;
}

interface Address {
  city: string;
  street: string;
  streetNum: string;
}
