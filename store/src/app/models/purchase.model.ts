export default interface Purchase {
  customerId: string;
  firstName: string;
  lastName: string;
  street: string;
  streetNo: string;
  city: {
    name: string;
  };
  email: string;
  phone: string;
}
