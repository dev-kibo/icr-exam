import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import Review from 'src/app/models/review.model';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css'],
})
export class ReviewComponent implements OnInit {
  @Input() review: Review;
  @Input() isDeletable: boolean;
  @Output() deleteComment = new EventEmitter<string>();
  isLoading = true;
  private _user: any;

  constructor(private firestore: AngularFirestore) {}

  ngOnInit(): void {
    this.firestore
      .collection('users', (ref) =>
        ref.where('id', '==', this.review.customerId)
      )
      .snapshotChanges()
      .subscribe((snapshots) => {
        const snapshot = snapshots[0];

        this._user = snapshot.payload.doc.data();
        this.isLoading = false;
      });
  }

  get userName() {
    return `${this._user.firstName} ${this._user.lastName}`;
  }

  getDate(date: string) {
    return new Date(date).toLocaleDateString('sr-ME', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    });
  }

  onCommentDelete() {
    this.deleteComment.emit(this.review.id);
  }
}
