import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import Product from 'src/app/models/product.model';
import Review from 'src/app/models/review.model';
import { AuthService } from 'src/app/services/auth.service';
import { OrderService } from 'src/app/services/order.service';
import { ReviewService } from 'src/app/services/review.service';

@Component({
  selector: 'app-product-reviews',
  templateUrl: './product-reviews.component.html',
  styleUrls: ['./product-reviews.component.css'],
})
export class ProductReviewsComponent implements OnInit {
  @Input() product: Product;
  @Output() commentDeleted = new EventEmitter<string>();
  @Output() commentLeft = new EventEmitter<object>();
  isReviewLeft: boolean = false;
  isProductPurchased: boolean = false;
  isLoading: boolean = true;
  customerId: string;

  constructor(
    private authService: AuthService,
    private reviewService: ReviewService,
    private orderService: OrderService
  ) {}

  ngOnInit(): void {
    this.reviewService
      .getReviewsForProduct(this.product.firebaseId)
      .subscribe((reviews) => {
        if (reviews.length > 0) {
          this.product.reviews = reviews;
        }
      });

    if (this.authService.isSignedIn) {
      this.customerId = this.authService.user.uid;

      this.orderService
        .getCustomerOrdersRaw(this.customerId)
        .subscribe((orders) => {
          orders.forEach((order) => {
            order.products.forEach((product) => {
              if (
                product.productId === this.product.firebaseId &&
                order.status === 'finished'
              ) {
                this.isProductPurchased = true;
              }
            });
          });
          this.isLoading = false;
        });
    }

    this.isLoading = false;
  }

  private getTotalRating() {
    return this.product.reviews.reduce((total, review) => {
      total += review.rating;

      return total;
    }, 0);
  }

  get averageReviews(): number {
    const total = this.getTotalRating();

    const average = total / this.product.reviews.length;

    return Math.round(average * 100) / 100;
  }

  getStarCount(star: number) {
    return this.product.reviews.reduce((total, review) => {
      if (review.rating === star) {
        total++;
      }

      return total;
    }, 0);
  }

  get hasReviews() {
    return this.product.reviews !== undefined;
  }

  get userReview() {
    return this.product.reviews.find((r) => r.customerId === this.customerId);
  }

  get hasReviewed() {
    if (this.product.reviews) {
      return this.product.reviews.find(
        (review) => review.customerId === this.customerId
      );
    }

    return false;
  }

  get currentUserId(): string {
    return this.customerId;
  }

  handleCommentLeft(comment) {
    this.commentLeft.emit(comment);
  }

  emitCommentDelete(id) {
    this.commentDeleted.emit(id);
  }
}
