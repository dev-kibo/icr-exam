import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Product from 'src/app/models/product.model';
import Review from 'src/app/models/review.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-leave-review',
  templateUrl: './leave-review.component.html',
  styleUrls: ['./leave-review.component.css'],
})
export class LeaveReviewComponent implements OnInit {
  @Input() product: Product;
  @Output() commentLeft = new EventEmitter<object>();

  rating: number;
  comment: string = null;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {}

  onComment() {
    const review: Review = {
      customerId: this.authService.user.uid,
      createdAt: new Date(Date.now()).toUTCString(),
      rating: this.rating,
      comment: this.comment,
    };

    this.commentLeft.emit(review);
  }
}
