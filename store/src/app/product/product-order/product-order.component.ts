import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import Product from 'src/app/models/product.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-product-order',
  templateUrl: './product-order.component.html',
  styleUrls: ['./product-order.component.css'],
})
export class ProductOrderComponent implements OnInit {
  @Input() product: Product;
  quantity: number = 1;

  constructor(
    private cartService: CartService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  get price() {
    return Math.round(this.product.price * this.quantity * 100) / 100;
  }

  onAddToCart(): void {
    this.cartService.addToCart(this.product.firebaseId, this.quantity);
    this.messageService.add({
      key: 'product',
      severity: 'success',
      summary: 'Cart',
      detail: 'Product added to cart.',
    });
  }
}
