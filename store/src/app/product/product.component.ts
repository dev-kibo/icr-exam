import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import Product from '../models/product.model';
import { map } from 'rxjs/operators';
import { ProductService } from '../services/product.service';
import { ReviewService } from '../services/review.service';
import Review from '../models/review.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  private _product: Product;
  isLoading = true;

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService,
    private reviewService: ReviewService
  ) {}

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.productService.getProductByFirebaseId(id).subscribe((result) => {
      this._product = result;
      this.isLoading = false;
    });
  }

  get product(): Product {
    return this._product;
  }

  handleCommentDeleted(reviewId: string) {
    this._product.reviews = this._product.reviews.filter(
      (r) => r.id !== reviewId
    );

    console.log(reviewId, 'PRODUCT COMP');
    this.reviewService.deleteReview(this._product.firebaseId, reviewId);

    this.messageService.add({
      severity: 'info',
      summary: 'Review',
      detail: 'Review Deleted.',
      key: 'review',
    });
  }

  handleCommentLeft(comment: Review) {
    if (!this._product.reviews) {
      this._product.reviews = [];
    }
    this._product.reviews.push(comment);

    this.reviewService.addReview(comment, this._product.firebaseId);
  }
}
