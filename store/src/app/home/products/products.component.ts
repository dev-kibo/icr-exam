import { Component, Input, OnInit } from '@angular/core';
import Product from 'src/app/models/product.model';

@Component({
  selector: 'app-home-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class RecommendedProductsComponent implements OnInit {
  @Input() products: Product[];

  constructor() {}

  ngOnInit(): void {}
}
