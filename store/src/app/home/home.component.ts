import { Component, OnInit } from '@angular/core';
import Product from '../models/product.model';
import { ProductService } from '../services/product.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  products: Product[];
  isLoading = true;

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService.getProducts().subscribe((products) => {
      this.products = products;
      this.isLoading = false;
    });
  }

  handleCategory(values: string[]) {
    if (values.length > 0) {
      this.productService
        .filterByCategories(values)
        .subscribe((results) => (this.products = results));
    } else {
      this.productService
        .getProducts()
        .subscribe((results) => (this.products = results));
    }
  }

  handleSize(sizes: number[]) {
    if (sizes.length > 0) {
      this.productService
        .filterBySizes(sizes)
        .subscribe((results) => (this.products = results));
    } else {
      this.productService
        .getProducts()
        .subscribe((results) => (this.products = results));
    }
  }

  handleQuantity(value: number) {
    this.productService
      .filterByQuantity(value)
      .subscribe((results) => (this.products = results));
  }

  handlePrice(prices: number[]) {
    this.productService
      .filterByPrices(prices)
      .subscribe((results) => (this.products = results));
  }

  handleDelivery(value: number) {
    if (value > 0) {
      this.productService
        .filterByDelivery(value)
        .subscribe((results) => (this.products = results));
    } else {
      this.productService
        .getProducts()
        .subscribe((results) => (this.products = results));
    }
  }

  handleReview(value: string) {
    const rating = Number.parseInt(value);
    if (rating !== 0) {
      this.products = this.products.filter((product) => {
        if (product.reviews === undefined || product.reviews === null) {
          return false;
        }
        if (product.reviews.length <= 0) {
          return false;
        }

        const totalRating = product.reviews.reduce((total, review) => {
          total += review.rating;
          return total;
        }, 0);
        const avgRating = Math.round(totalRating / product.reviews.length);

        return rating === avgRating;
      });
    } else {
      this.productService.getProducts().subscribe((results) => {
        this.products = results;
      });
    }
  }

  handleSearch(name: string) {
    if (name.length > 0) {
      this.productService
        .searchByName(name)
        .subscribe((results) => (this.products = results));
    } else {
      this.productService
        .getProducts()
        .subscribe((results) => (this.products = results));
    }
  }
}
