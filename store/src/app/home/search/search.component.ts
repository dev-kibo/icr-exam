import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  @Output() searchFilter: EventEmitter<string> = new EventEmitter<string>();

  searchValue: string;

  constructor() {}

  ngOnInit(): void {}

  emitSearch() {
    this.searchFilter.emit(this.searchValue);
  }
}
