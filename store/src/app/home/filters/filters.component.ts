import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-home-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css'],
})
export class FiltersComponent implements OnInit {
  @Output() categoryFilter = new EventEmitter<string[]>();
  @Output() sizeFilter = new EventEmitter<number[]>();
  @Output() quantityFilter = new EventEmitter<number>();
  @Output() priceFilter = new EventEmitter<number[]>();
  @Output() deliveryFilter = new EventEmitter<number>();
  @Output() reviewFilter = new EventEmitter<number>();

  selectedCategories: string[] = [];
  selectedSizes: string[] = [];
  selectedPrices: number[] = [];

  quantity: number = 1;
  maxQuantity: number = 10001;

  priceValues: number[] = [1, 501];
  priceMin: number = this.priceValues[0];
  priceMax: number = this.priceValues[1];

  deliveryTime: any[] = [
    { name: 'Select delivery time', value: 0 },
    { name: '1 day', value: 1 },
    { name: '3 days', value: 3 },
    { name: '7 days', value: 7 },
    { name: '14 days', value: 14 },
    { name: '30 days', value: 30 },
  ];
  deliveryTimeValue: number;

  reviewValue: number = 0;

  constructor() {}

  ngOnInit(): void {}

  emitCategory() {
    this.categoryFilter.emit(this.selectedCategories);
  }

  emitSize() {
    const sizes: number[] = this.selectedSizes.map((s) => Number.parseInt(s));
    this.sizeFilter.emit(sizes);
  }

  emitQuantity() {
    this.quantityFilter.emit(this.quantity);
  }

  emitPrices() {
    this.priceFilter.emit(this.priceValues);
  }

  emitDelivery() {
    this.deliveryFilter.emit(this.deliveryTimeValue);
  }

  emitReview() {
    this.reviewFilter.emit(this.reviewValue);
  }
}
