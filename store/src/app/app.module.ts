import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrimeNgModule } from './primeng.module';
import { HomeComponent } from './home/home.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SearchComponent } from './home/search/search.component';
import { FiltersComponent } from './home/filters/filters.component';
import { NewProductsComponent } from './home/new-products/new-products.component';
import { RecommendedProductsComponent } from './home/products/products.component';
import { ProductComponent } from './product/product.component';
import { ProductGalleryComponent } from './product/product-gallery/product-gallery.component';
import { ProductOrderComponent } from './product/product-order/product-order.component';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { ProductReviewsComponent } from './product/product-reviews/product-reviews.component';
import { CartComponent } from './cart/cart.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import { PurchaseComponent } from './purchase/purchase.component';
import { DeliveryComponent } from './purchase/delivery/delivery.component';
import { SummaryComponent } from './purchase/summary/summary.component';
import { PayComponent } from './purchase/pay/pay.component';
import { ProfileComponent } from './profile/profile.component';
import { OrdersComponent } from './orders/orders.component';
import { LeaveReviewComponent } from './product/product-reviews/leave-review/leave-review.component';
import { ReviewComponent } from './product/product-reviews/review/review.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { SpinnerComponent } from './spinner/spinner.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignInComponent,
    SignUpComponent,
    ProductCardComponent,
    NavigationComponent,
    SearchComponent,
    FiltersComponent,
    NewProductsComponent,
    RecommendedProductsComponent,
    ProductComponent,
    ProductGalleryComponent,
    ProductOrderComponent,
    ProductDetailsComponent,
    ProductReviewsComponent,
    CartComponent,
    CartItemComponent,
    PurchaseComponent,
    DeliveryComponent,
    SummaryComponent,
    PayComponent,
    ProfileComponent,
    OrdersComponent,
    LeaveReviewComponent,
    ReviewComponent,
    SpinnerComponent,
    EditProfileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PrimeNgModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    HttpClientModule,
  ],
  providers: [MessageService, ConfirmationService],
  bootstrap: [AppComponent],
})
export class AppModule {}
