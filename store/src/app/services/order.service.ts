import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import Order from '../models/order.model';
import { map } from 'rxjs/operators';
import { ProductService } from './product.service';
import { ReviewService } from './review.service';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(
    private firestore: AngularFirestore,
    private productService: ProductService,
    private reviewService: ReviewService
  ) {}

  private mapOrdersToHaveId() {
    return map((actions: any) => {
      return actions.map((action) => {
        const data: any = action.payload.doc.data();
        const id = action.payload.doc.id;
        const order: Order = {
          ...data,
          id: id,
        };
        return order;
      });
    });
  }

  private getProductsForEachOrder() {
    return map((orders: any) => {
      return orders.map((order) => {
        const products = order.products;
        order.products = [];

        products.forEach((product) => {
          this.productService
            .getProductByFirebaseIdOnce(product.productId)
            .subscribe((result) => {
              this.reviewService
                .getReviewsForProduct(product.productId)
                .subscribe((reviews) => {
                  order.products.push({
                    ...result,
                    reviews: reviews,
                    quantity: product.quantity,
                  });
                });
              // order.products.push({
              //   ...result,
              //   quantity: product.quantity,
              // });
            });
        });

        return order;
      });
    });
  }

  getCustomerOrders(customerId: string): Observable<Order[]> {
    return this.firestore
      .collection<Order>('orders', (ref) =>
        ref.where('customerId', '==', customerId)
      )
      .snapshotChanges()
      .pipe(this.mapOrdersToHaveId(), this.getProductsForEachOrder());
  }

  getCustomerOrdersRaw(customerId: string): Observable<Order[]> {
    return this.firestore
      .collection('orders', (ref) => ref.where('customerId', '==', customerId))
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const id = a.payload.doc.id;
            const data: any = a.payload.doc.data();

            return { id, ...data };
          })
        )
      );
  }
}
