import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import Product from '../models/product.model';
import Review from '../models/review.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ReviewService {
  constructor(private firestore: AngularFirestore) {}

  addReview(review: Review, productFirebaseId: string) {
    this.firestore
      .collection('products')
      .doc<Product>(productFirebaseId)
      .collection<Review>('reviews')
      .add(review);
  }

  deleteReview(productId: string, reviewId: string) {
    this.firestore
      .collection<Product>('products')
      .doc<Product>(productId)
      .collection<Review>('reviews')
      .doc<Review>(reviewId)
      .delete();
  }

  getReviewsForProduct(productId: string): Observable<Review[]> {
    return this.firestore
      .doc(`products/${productId}`)
      .collection<Review>('reviews')
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const id = a.payload.doc.id;
            const data = a.payload.doc.data();

            return { id, ...data };
          })
        )
      );
  }
}
