import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  DocumentChangeAction,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import Product from '../models/product.model';
import { map } from 'rxjs/operators';
import { ReviewService } from './review.service';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(
    private firestore: AngularFirestore,
    private reviewService: ReviewService
  ) {}

  private mapResults() {
    return map((actions: any) =>
      actions.map((a) => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;

        return { firebaseId: id, ...data };
      })
    );
  }

  private mapResult() {
    return map((result: any) => {
      const data = result.payload.data();
      const id = result.payload.id;

      return { firebaseId: id, ...data };
    });
  }

  private mapReviewsToProducts() {
    return map((products: any[]) => {
      return products.map((product) => {
        this.reviewService
          .getReviewsForProduct(product.firebaseId)
          .subscribe((reviews) => {
            product.reviews = reviews;
          });

        return product;
      });
    });
  }

  getProducts(): Observable<Product[]> {
    return this.firestore
      .collection<Product>('products')
      .snapshotChanges()
      .pipe(this.mapResults(), this.mapReviewsToProducts());
  }

  getProductByFirebaseIdOnce(firebaseId: string) {
    return this.firestore
      .collection('products')
      .doc(firebaseId)
      .get()
      .pipe(
        map((snapshot) => {
          const id = snapshot.id;
          const data: any = snapshot.data();

          return { firebaseId: id, ...data };
        })
      );
  }

  getProductByFirebaseId(firebaseId: string): Observable<Product> {
    return this.firestore
      .collection<Product>('products')
      .doc(firebaseId)
      .snapshotChanges()
      .pipe(this.mapResult());
  }

  filterByCategories(categories: string[]): Observable<Product[]> {
    return this.firestore
      .collection<Product>('products', (ref) =>
        ref.where('category', 'in', categories)
      )
      .snapshotChanges()
      .pipe(this.mapResults(), this.mapReviewsToProducts());
  }

  filterBySizes(sizes: number[]): Observable<Product[]> {
    return this.firestore
      .collection<Product>('products', (ref) => ref.where('size', 'in', sizes))
      .snapshotChanges()
      .pipe(this.mapResults(), this.mapReviewsToProducts());
  }

  filterByQuantity(quantity: number): Observable<Product[]> {
    return this.firestore
      .collection<Product>('products', (ref) =>
        ref.where('stock', '>=', quantity)
      )
      .snapshotChanges()
      .pipe(this.mapResults(), this.mapReviewsToProducts());
  }

  filterByPrices(prices: number[]): Observable<Product[]> {
    return this.firestore
      .collection<Product>('products', (ref) =>
        ref.where('price', '>=', prices[0]).where('price', '<=', prices[1])
      )
      .snapshotChanges()
      .pipe(this.mapResults(), this.mapReviewsToProducts());
  }

  filterByDelivery(deliveryDays: number): Observable<Product[]> {
    return this.firestore
      .collection<Product>('products', (ref) =>
        ref.where('deliveryDays', '==', deliveryDays)
      )
      .snapshotChanges()
      .pipe(this.mapResults(), this.mapReviewsToProducts());
  }

  filterByReviews(value: number) {
    // return this.firestore
    //   .collection<Product>('products')
    //   .snapshotChanges()
    //   .pipe(
    //     this.mapResults(),
    //     this.mapReviewsToProducts(),
    //     map((products) => {
    //       return products.filter((product) => {
    //         console.log(product.reviews);
    //         if (product.reviews === undefined || product.reviews === null) {
    //           return false;
    //         }
    //         if (product.reviews.length <= 0) {
    //           return false;
    //         }
    //         console.log(product);
    //         const totalRating = product.reviews.reduce((total, review) => {
    //           total += review.rating;
    //           return total;
    //         }, 0);
    //         const avgRating = Math.round(totalRating / product.reviews.length);
    //         return value === avgRating;
    //       });
    //     })
    //   );
  }

  getLatestProductsOrderByDesc(limit: number = 12): Observable<Product[]> {
    return this.firestore
      .collection<Product>('products', (ref) =>
        ref.orderBy('createdAt', 'desc').limit(limit)
      )
      .snapshotChanges()
      .pipe(this.mapResults(), this.mapReviewsToProducts());
  }

  searchByName(name: string): Observable<Product[]> {
    return this.firestore
      .collection<Product>('products', (ref) =>
        ref.orderBy('name').startAt(name).limit(1)
      )
      .snapshotChanges()
      .pipe(this.mapResults());
  }
}
