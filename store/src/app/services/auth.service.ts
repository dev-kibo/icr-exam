import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { first, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _user: firebase.default.User;

  constructor(
    private fireAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private router: Router
  ) {
    const user = localStorage.getItem('user');
    if (user) {
      this._user = JSON.parse(user);
    }
    fireAuth.authState.subscribe((user) => {
      if (user) {
        this._user = user;
        localStorage.setItem('user', JSON.stringify(user));
      } else {
        localStorage.setItem('user', null);
      }
    });
  }

  get user() {
    return this._user;
  }

  async createUserWithPassword(customer, email, password) {
    const userCred = await this.fireAuth.createUserWithEmailAndPassword(
      email,
      password
    );

    customer.id = userCred.user.uid;
    this.firestore.collection('users').add(customer);

    this.router.navigate(['/sign-in']);
  }

  async signInWithEmailAndPassword(email: string, password: string) {
    await this.fireAuth.signInWithEmailAndPassword(email, password);

    this.router.navigate(['/']);
  }

  async signOut() {
    await this.fireAuth.signOut();
  }

  get isSignedIn(): boolean {
    return JSON.parse(localStorage.getItem('user')) ? true : false;
  }

  get userDetails() {
    return this.firestore
      .collection('users', (ref) => ref.where('id', '==', this._user.uid))
      .valueChanges();
  }
}
