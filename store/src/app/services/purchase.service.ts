import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { OrderProduct } from '../models/order-product.model';
import Order from '../models/order.model';
import Purchase from '../models/purchase.model';
import { AuthService } from './auth.service';
import { CartService } from './cart.service';

@Injectable({
  providedIn: 'root',
})
export class PurchaseService {
  private _purchase: Purchase = {
    city: {
      name: '',
    },
    email: '',
    customerId: '',
    firstName: '',
    lastName: '',
    phone: '',
    street: '',
    streetNo: '',
  };

  constructor(
    private authService: AuthService,
    private firestore: AngularFirestore,
    private cartService: CartService,
    private router: Router
  ) {}

  get purchase(): Purchase {
    return this._purchase;
  }

  set purchase(purchase: Purchase) {
    this._purchase = purchase;
  }

  doPurchase() {
    const user = this.authService.user;

    const order: Order = {
      customerId: user.uid,
      products: this.cartService.cart.products.map((product) => {
        const orderProduct: OrderProduct = {
          productId: product.firebaseId,
          quantity: product.quantity,
        };
        return orderProduct;
      }),
      totalPrice: this.cartService.cart.totalPrice,
      createdAt: new Date(Date.now()).toUTCString(),
      status: 'inProgress',
      deliveryAddress: {
        city: this._purchase.city.name,
        street: this._purchase.street,
        streetNum: this._purchase.streetNo,
      },
    };

    console.log(order);
    this.firestore.collection<Order>('orders').add(order);
    this.router.navigateByUrl('/purchase/summary');
  }
}
