import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import ShoppingCart from '../models/shopping-cart.model';
import { AuthService } from './auth.service';
import { ProductService } from './product.service';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private _cart: ShoppingCart = null;

  private _numOfCartItems: BehaviorSubject<number> = new BehaviorSubject(
    this._cart ? this._cart.products.length : 0
  );

  constructor(
    private authService: AuthService,
    private productService: ProductService
  ) {
    const serializedCart = localStorage.getItem('cart');

    if (serializedCart !== null) {
      const cart: ShoppingCart = JSON.parse(serializedCart);

      this._cart = cart;
    }
  }

  async addToCart(productId: string, quantity: number = 1) {
    if (this._cart === null) {
      this._cart = {
        customerId: this.authService.isSignedIn
          ? this.authService.user.uid
          : null,
        products: [],
        totalPrice: 0,
      };
    }
    const product = this._cart.products.find((p) => p.firebaseId === productId);

    if (product) {
      this._cart.products = this.updateCartProductQuantity(product.firebaseId);

      localStorage.setItem('cart', JSON.stringify(this._cart));
      // emit new number of items in the cart
      this._numOfCartItems.next(this._cart.products.length);
    } else {
      this.productService
        .getProductByFirebaseIdOnce(productId)
        .subscribe((product) => {
          this._cart.products.push({ ...product, quantity: quantity });
          this._cart.totalPrice = this.calculateTotalPrice();

          localStorage.setItem('cart', JSON.stringify(this._cart));
          // emit new number of items in the cart
          this._numOfCartItems.next(this._cart.products.length);
        });
    }
  }

  private updateCartProductQuantity(id: string) {
    return this._cart.products.map((product) => {
      if (product.firebaseId === id) {
        product.quantity++;
      }

      return product;
    });
  }

  removeFromCart(productId: string) {
    this._cart.products = this._cart.products.filter(
      (p) => p.firebaseId !== productId
    );
    this._cart.totalPrice = this.calculateTotalPrice();

    localStorage.setItem('cart', JSON.stringify(this._cart));
    // emit
    this._numOfCartItems.next(this._cart.products.length);
  }

  changeQuantity(productId: string, quantity: number) {
    this._cart.products.find(
      (p) => p.firebaseId === productId
    ).quantity = quantity;
    this._cart.totalPrice = this.calculateTotalPrice();

    localStorage.setItem('cart', JSON.stringify(this._cart));
  }

  private calculateTotalPrice(): number {
    const totalPrice = this._cart.products.reduce((totalPrice, product) => {
      totalPrice = totalPrice + product.price * product.quantity;

      return totalPrice;
    }, 0);

    return Math.round(totalPrice * 100) / 100;
  }

  emptyCart(): void {
    this._cart = {
      customerId: null,
      products: [],
      totalPrice: 0,
    };

    this._numOfCartItems.next(0);
    localStorage.removeItem('cart');
  }

  get cart(): ShoppingCart {
    return this._cart;
  }

  get numOfCartItems(): Observable<number> {
    return this._numOfCartItems;
  }
}
