import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { PrimeNGConfig } from 'primeng/api';

import PRODUCTS from './mock-data/products.mock';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  userId: string = null;

  constructor(
    private primengConfig: PrimeNGConfig,
    private firestore: AngularFirestore
  ) {
    const ref = this.firestore.collection('products');

    // seed products
    // dont log when do this
    // PRODUCTS.forEach((product, index) => {
    //   ref.add(product);
    //   console.log(`added ${index}`);
    // });
    // ref.valueChanges().subscribe((res) => console.log(res.length));
  }

  ngOnInit() {
    this.primengConfig.ripple = true;
    // var docWidth = document.documentElement.offsetWidth;

    // [].forEach.call(document.querySelectorAll('*'), function (el) {
    //   if (el.offsetWidth > docWidth) {
    //     console.log(el);
    //   }
    // });
  }

  title = 'shop';
}
