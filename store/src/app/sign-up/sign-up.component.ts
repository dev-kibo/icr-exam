import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Customer from '../models/customer.model';
import { AuthService } from '../services/auth.service';
import CITIES from '../values/city.value';
import PRODUCT_TYPES from '../values/product-types.value';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent {
  private _cities: any[];
  private _products: any[];
  errorMessage: any[] = [];

  registerForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    street: new FormControl('', Validators.required),
    streetNo: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    confirmPassword: new FormControl('', Validators.required),
    favoriteProducts: new FormControl('', Validators.required),
  });

  constructor(private authService: AuthService) {
    this._cities = CITIES;
    this._products = PRODUCT_TYPES;
  }

  onConfirmPassword(value: string) {
    const password = this.password.value;
    const confirmPassword = this.confirmPassword;

    if (password !== value) {
      confirmPassword.setErrors({ isInvalid: true });
    } else if (password === value) {
      confirmPassword.markAsPristine();
    }
  }

  async onSubmit(event: Event) {
    event.preventDefault();
    try {
      const customer = {
        firstName: this.firstName.value,
        lastName: this.lastName.value,
        phone: this.phone.value,
        address: {
          city: this.city.value.name,
          street: this.street.value,
          streetNumber: this.streetNo.value,
        },
        favoriteProducts: this.favoriteProducts.value,
      };

      await this.authService.createUserWithPassword(
        customer,
        this.email.value,
        this.password.value
      );
    } catch (error) {
      console.dir(error);

      this.errorMessage = [];
      this.errorMessage.push({
        severity: 'error',
        summary: 'Error',
        detail: error.message,
      });
    }
  }

  get firstName() {
    return this.registerForm.get('firstName');
  }

  get lastName() {
    return this.registerForm.get('lastName');
  }

  get phone() {
    return this.registerForm.get('phone');
  }

  get street() {
    return this.registerForm.get('street');
  }

  get streetNo() {
    return this.registerForm.get('streetNo');
  }

  get city() {
    return this.registerForm.get('city');
  }

  get cities(): any[] {
    return this._cities;
  }

  get products(): any[] {
    return this._products;
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  get favoriteProducts() {
    return this.registerForm.get('favoriteProducts');
  }
}
