import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent {
  errorMessage: any[] = [];

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });

  constructor(private authService: AuthService) {}

  async onSubmit(event: Event) {
    event.preventDefault();

    try {
      await this.authService.signInWithEmailAndPassword(
        this.email.value,
        this.password.value
      );
    } catch (error) {
      console.dir(error);
      this.errorMessage = [];
      this.errorMessage.push({
        severity: 'error',
        summary: 'Error',
        detail: error.message,
      });
    }
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }
}
