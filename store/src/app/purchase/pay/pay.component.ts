import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PurchaseService } from 'src/app/services/purchase.service';

@Component({
  selector: 'app-purchase-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css'],
})
export class PayComponent implements OnInit {
  payForm = new FormGroup({
    name: new FormControl('', Validators.required),
    number: new FormControl('', Validators.required),
    date: new FormControl('', Validators.required),
    cvv: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router,
    private purchaseService: PurchaseService
  ) {}

  ngOnInit(): void {}

  onNext(): void {
    this.purchaseService.doPurchase();
  }
  onPrev(): void {
    this.router.navigate(['/purchase/delivery']);
  }

  get name() {
    return this.payForm.get('name');
  }
  get number() {
    return this.payForm.get('number');
  }
  get date() {
    return this.payForm.get('date');
  }
  get cvv() {
    return this.payForm.get('cvv');
  }
}
