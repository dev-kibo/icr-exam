import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css'],
})
export class PurchaseComponent implements OnInit {
  activeIndex = 1;
  items: MenuItem[];

  constructor() {}

  ngOnInit(): void {
    this.items = [
      { label: 'Delivery address', routerLink: 'delivery' },
      { label: 'Payment', routerLink: 'pay' },
      { label: 'Summary', routerLink: 'summary' },
    ];
  }
}
