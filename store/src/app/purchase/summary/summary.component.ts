import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Purchase from 'src/app/models/purchase.model';
import { CartService } from 'src/app/services/cart.service';
import { PurchaseService } from 'src/app/services/purchase.service';

@Component({
  selector: 'app-purchase-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css'],
})
export class SummaryComponent implements OnInit {
  constructor(private cartService: CartService, private router: Router) {}

  ngOnInit(): void {
    this.cartService.emptyCart();
  }

  goToHome() {
    this.router.navigate(['/']);
  }
}
