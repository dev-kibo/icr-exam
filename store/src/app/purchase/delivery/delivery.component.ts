import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import City from 'src/app/models/city.model';
import { AuthService } from 'src/app/services/auth.service';
import { PurchaseService } from 'src/app/services/purchase.service';
import CITIES from 'src/app/values/city.value';

@Component({
  selector: 'app-purchase-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.css'],
})
export class DeliveryComponent {
  private purchase;

  isDeliveredToProfileAddress: boolean = false;
  cities: City[] = CITIES;

  purchaseForm;

  constructor(
    private purchaseService: PurchaseService,
    private router: Router,
    private authService: AuthService
  ) {
    this.purchase = this.purchaseService.purchase;

    this.purchaseForm = new FormGroup({
      firstName: new FormControl(this.purchase.firstName, Validators.required),
      lastName: new FormControl(this.purchase.lastName, Validators.required),
      phone: new FormControl(this.purchase.phone, Validators.required),
      street: new FormControl(this.purchase.street, Validators.required),
      streetNo: new FormControl(this.purchase.streetNo, Validators.required),
      city: new FormControl(this.purchase.city, Validators.required),
      email: new FormControl(this.purchase.email, [
        Validators.required,
        Validators.email,
      ]),
    });
  }

  handleChange(event) {
    if (event.checked) {
      this.purchaseForm.disable();
    } else {
      this.purchaseForm.enable();
    }
  }

  onNext(): void {
    if (!this.isDeliveredToProfileAddress) {
      this.setPurchaseToNewCustomerAddress();
    } else {
      this.setPurchaseDetailsToCurrentCustomerAddress();
    }
    this.router.navigate(['purchase/pay']);
  }

  private setPurchaseToNewCustomerAddress() {
    const form = this.purchaseForm;

    const user = this.authService.user;

    this.purchaseService.purchase = {
      city: {
        name: form.get('city').value,
      },
      email: form.get('email').value,
      customerId: user.uid,
      firstName: form.get('firstName').value,
      lastName: form.get('lastName').value,
      phone: form.get('phone').value,
      street: form.get('street').value,
      streetNo: form.get('streetNo').value,
    };
  }

  private setPurchaseDetailsToCurrentCustomerAddress() {
    const user = this.authService.user;

    this.authService.userDetails.subscribe((customers) => {
      const customer: any = customers[0];
      this.purchaseService.purchase = {
        city: {
          name: customer.address.city,
        },
        email: user.email,
        customerId: customer.id,
        firstName: customer.firstName,
        lastName: customer.lastName,
        phone: customer.phone,
        street: customer.address.street,
        streetNo: customer.address.streetNumber,
      };
    });
  }

  get firstName() {
    return this.purchaseForm.get('firstName');
  }

  get lastName() {
    return this.purchaseForm.get('lastName');
  }

  get phone() {
    return this.purchaseForm.get('phone');
  }

  get street() {
    return this.purchaseForm.get('street');
  }

  get streetNo() {
    return this.purchaseForm.get('streetNo');
  }

  get city() {
    return this.purchaseForm.get('city');
  }

  get email() {
    return this.purchaseForm.get('email');
  }
}
