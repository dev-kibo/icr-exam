import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import { DeliveryComponent } from './purchase/delivery/delivery.component';
import { PayComponent } from './purchase/pay/pay.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SummaryComponent } from './purchase/summary/summary.component';
import { OrdersComponent } from './orders/orders.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path: 'sign-in',
    component: SignInComponent,
    pathMatch: 'full',
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    pathMatch: 'full',
  },
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'product/:id',
    component: ProductComponent,
  },
  {
    path: 'my-cart',
    component: CartComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'purchase',
    component: PurchaseComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'delivery',
        component: DeliveryComponent,
      },
      {
        path: 'pay',
        component: PayComponent,
      },
      {
        path: 'summary',
        component: SummaryComponent,
      },
    ],
  },
  {
    path: 'my-orders',
    component: OrdersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
