import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import ShoppingCart from '../models/shopping-cart.model';
import { AuthService } from '../services/auth.service';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  private _cart: ShoppingCart;

  constructor(
    private cartService: CartService,
    private authService: AuthService,
    private router: Router
  ) {
    this._cart = this.cartService.cart;
  }

  ngOnInit(): void {}

  get products() {
    return this._cart.products;
  }

  get cart() {
    return this._cart;
  }

  get isEmpty(): boolean {
    return this._cart ? this._cart.products.length <= 0 : true;
  }

  handleRemoveFromCart(productId: string) {
    this.cartService.removeFromCart(productId);
  }

  handleQuantity(productId: string, value: number): void {
    this.cartService.changeQuantity(productId, value);
  }

  async onPurchase() {
    if (this.cartService.cart.customerId) {
      this.cartService.cart.customerId = this.authService.user.uid;
    }

    this.router.navigate(['/purchase/delivery']);
  }
}
