import { Component, Input, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css'],
})
export class CartItemComponent {
  @Input() product: any;

  constructor(private cartService: CartService) {}

  handleRemoveFromCart(productId: string) {
    this.cartService.removeFromCart(productId);
  }

  handleQuantity(productId: string, value: number): void {
    this.cartService.changeQuantity(productId, value);
  }
}
