import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { map } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { OrderService } from '../services/order.service';
import { ReviewService } from '../services/review.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})
export class OrdersComponent implements OnInit {
  private _orders = [];
  private user: firebase.default.User;

  constructor(
    private authService: AuthService,
    private orderService: OrderService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private firestore: AngularFirestore,
    private reviewService: ReviewService
  ) {}

  ngOnInit(): void {
    this.user = this.authService.user;

    this.orderService.getCustomerOrders(this.user.uid).subscribe((results) => {
      this._orders = results;
    });
  }

  formatDate(date: string): string {
    return new Date(date).toLocaleDateString('sr', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    });
  }

  calculateTotalPrice(price, quantity) {
    return Math.round(price * quantity * 100) / 100;
  }

  getStatus(status: string): string {
    switch (status) {
      case 'inProgress':
        return 'PENDING';
      case 'finished':
        return 'DELIVERED';
      case 'cancelled':
        return 'CANCELLED';
    }
  }

  goToProduct(productId: string) {
    this.router.navigate(['/product', productId]);
  }

  cancelOrder(orderId: string) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to cancel this order?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.firestore.collection('orders').doc(orderId).update({
          status: 'cancelled',
        });
      },
      rejectButtonStyleClass: 'p-button-text p-button-plain',
    });
  }

  deleteOrder(orderId: string) {
    this.firestore.collection('orders').doc(orderId).delete();
  }

  getExpectedDeliveryTime(orderId: string) {
    const order = this._orders.find((o) => o.id === orderId);
    // const cart = this.getCartForOrder(order.id);

    const totalDeliveryTime = order.products.reduce((total, product) => {
      total += product.deliveryDays;

      return total;
    }, 0);

    return Math.round(totalDeliveryTime / order.products.length);
  }

  isProductRated(productId) {
    let product;
    for (const order of this._orders) {
      for (const p of order.products) {
        if (p.firebaseId === productId) {
          product = p;
          break;
        }
      }
    }

    if (!product.reviews) {
      return false;
    }

    const review = product.reviews.find((r) => r.customerId === this.user.uid);

    if (review === undefined) {
      return false;
    }

    return true;
  }

  getProductRating(productId) {
    let product;
    for (const order of this._orders) {
      for (const p of order.products) {
        if (p.firebaseId === productId) {
          product = p;
          break;
        }
      }
    }

    const review = product.reviews.find((r) => r.customerId === this.user.uid);

    return review.rating;
  }

  getDeliveryAddress(orderId: string) {
    const order = this._orders.find((o) => o.id === orderId);

    return `${order.deliveryAddress.street} ${order.deliveryAddress.streetNum}, ${order.deliveryAddress.city}`;
  }

  private getCartForOrder(orderId: string) {
    return this._orders.find((o) => o.id === orderId).cart;
  }

  getTotalPriceForOrder(orderId: string) {
    const cart = this.getCartForOrder(orderId);

    return cart.products.reduce((total, product) => {
      total += product.price * product.quantity;

      return total;
    }, 0);
  }

  get orders() {
    return this._orders;
  }
}
