import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Subscription } from 'rxjs';
import Customer from '../models/customer.model';
import { AuthService } from '../services/auth.service';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit, OnDestroy {
  items: MenuItem[];
  cartItems: number = 0;
  private subscriber: Subscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private cartService: CartService
  ) {}

  ngOnDestroy(): void {
    this.subscriber.unsubscribe();
  }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Profile',
        icon: 'pi pi-fw pi-user-edit',
        routerLink: 'profile',
      },
      { label: 'Orders', icon: 'pi pi-fw pi-list', routerLink: 'my-orders' },
      { separator: true },
      {
        label: 'Sign Out',
        icon: 'pi pi-fw pi-sign-out',
        command: () => this.onSignOut(),
      },
    ];

    this.subscriber = this.cartService.numOfCartItems.subscribe((value) => {
      this.cartItems = value;
    });

    if (this.cartService.cart?.products) {
      this.cartItems = this.cartService.cart.products.length;
    }
  }

  get isSignedIn(): boolean {
    return this.authService.isSignedIn;
  }

  onSignOut(): void {
    this.authService.signOut();
  }
}
