// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCfHJ7UpD55Qakv_YajXFWQF2BNifX26n4',
    authDomain: 'icr-shop.firebaseapp.com',
    databaseURL:
      'https://icr-shop-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'icr-shop',
    storageBucket: 'icr-shop.appspot.com',
    messagingSenderId: '933258983536',
    appId: '1:933258983536:web:d57c94e45ddfe44dd99cb5',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
